import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import Header from './Components/Header/index';
import Routes from './Routes';

function App() {
  return (
    <Router>
      <div className="app">
        <Header />
        <Routes />
      </div>
    </Router>
  );
}

export default App;
