import React from 'react';
import './text.css';

const Text = {};

const BaseText = ({ TextComponent, children, multiLine, style, className = '', alt, colored, capitalized, boxed }) => {
    return !boxed ? (<>
        <TextComponent style={style} className={`${colored ? 'color-text ' : ''}${alt ? 'alt ' : ''}${className}${capitalized ? 'capitalized ' : ''}`}>
            {children}
        </TextComponent>
        {multiLine && (
            <TextComponent className='multiline-spacing-text'>
                {new Array(multiLine).fill(null).map(() => <br />)}
            </TextComponent>
        )}
    </>) : (<div className='text-box'>
    <TextComponent style={style} className={`${colored ? 'color-text ' : ''}${alt ? 'alt ' : ''}${className}${capitalized ? 'capitalized ' : ''}`}>
        {children}
    </TextComponent>
    {multiLine && (
        <TextComponent className='multiline-spacing-text'>
            {new Array(multiLine).fill(null).map(() => <br />)}
        </TextComponent>
    )}
</div>)
}

const wrapBaseText = wrapper => props => <BaseText TextComponent={wrapper} {...props} />

Text.H1 = wrapBaseText(({ children, className, style, colored }) => <h1 className={className} style={style} colored={colored}>{children}</h1>)
Text.H2 = wrapBaseText(({ children, className, style, colored }) => <h2 className={className} style={style} colored={colored}>{children}</h2>)
Text.H3 = wrapBaseText(({ children, className, style, colored }) => <h3 className={className} style={style} colored={colored}>{children}</h3>)
Text.H4 = wrapBaseText(({ children, className, style, colored }) => <h4 className={className} style={style} colored={colored}>{children}</h4>)
Text.H5 = wrapBaseText(({ children, className, style, colored }) => <h5 className={className} style={style} colored={colored}>{children}</h5>)
Text.H6 = wrapBaseText(({ children, className, style, colored }) => <h6 className={className} style={style} colored={colored}>{children}</h6>)
Text.P = wrapBaseText(({ children, className, style, colored }) => <p className={className} style={style} colored={colored}>{children}</p>)

export default Text;