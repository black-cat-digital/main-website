import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { matchPath } from 'react-router';

import { ReactComponent as Hamburger } from '../../icons/Hamburger.svg';
import { ReactComponent as Logo } from '../../icons/Logo.svg';
import TypeText from '../TypeText';
import Menu from '../Menu';
import Text from '../Text';
import './header.css';

const Header = () => {
    const [headerTitle, setHeaderTitle] = useState('');
    const [menuOpen, setMenuOpen] = useState(false);

    const locationTitleMap = useRef({
        '/services': 'Services',
        '/contact': 'Contact',
        '/about': 'About',
        '/work': 'Work',
        '/': '',
    }).current

    const location = useLocation();

    const toggleMenu = useCallback(() => {
        setMenuOpen(!menuOpen);
    }, [menuOpen])

    useEffect(() => {
        Object.entries(locationTitleMap).forEach(([routeMatch, title]) => {
            if (!!matchPath(location.pathname, {
                path: routeMatch,
                exact: true,
                strict: true
            })) {
                setHeaderTitle(title);
            }
        })
    }, [location, locationTitleMap])
    
    return (
        <div id='header'>
            <Link to='/'>
                <div id='logo-container'>
                    <Logo id='header-logo' />
                </div>
            </Link>
            <Text.H6 style={{flex: 1, textAlign: 'center'}}>
                <TypeText text={headerTitle} />
            </Text.H6>
            <Hamburger id='header-hamburger' onClick={toggleMenu} />
            <Menu isOpen={menuOpen} closeMenu={toggleMenu} />
        </div>
    )
}

export default Header;