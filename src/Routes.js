import { About, Contact, Home, Service, Services, Work, Works } from "./Pages";
import { Switch, Route, useLocation } from "react-router";
import { useLayoutEffect } from "react";
const Routes = () => {
    const location = useLocation();

    useLayoutEffect(() => {
        window.scrollTo(0, 0);
    }, [location.pathname]);

    return (
        <Switch>
            <Route path='/main-website/work'>
                <Works />
            </Route>
            <Route path='/main-website/work/:projectID'>
                <Work />
            </Route>
            <Route path='/main-website/services/:serviceID'>
                <Service />
            </Route>
            <Route path='/main-website/services'>
                <Services />
            </Route>
            <Route path='/main-website/about'>
                <About />
            </Route>
            <Route path='/main-website/contact'>
                <Contact />
            </Route>
            <Route path='/main-website/'>
                <Home />
            </Route>
        </Switch>
)
}

export default Routes;