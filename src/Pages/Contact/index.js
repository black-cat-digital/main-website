import React from 'react';
import PageContainer from '../../Components/PageContainer';
import Text from '../../Components/Text';
import Button from '../../Components/Button';
import Input from '../../Components/Input';

const Contact = () => {

    const handleSubmit = e => {
    }

    return (
        <PageContainer>
            <Text.H4 colored>Let's Connect!</Text.H4>
            <Text.P>There is no question too big or small. Let us know what’s on your mind and we will get back to you.</Text.P>
            <form action="mailto:morganb816@gmail.com" method="post" encType="multipart/form-data" name="EmailFrom" onSubmit={handleSubmit}>
                <Input type='text' placeholder='Name' />
                <Input type='text' placeholder='Email Address' />
                <Input.Multiline type='text' placeholder='Comments, Questions, Favorite Movies?' rows={6}/>
                <Button.Outlined>Submit</Button.Outlined>
            </form>
        </PageContainer>
    )
}

export default Contact;