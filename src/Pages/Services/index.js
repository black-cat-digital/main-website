import React, { useState } from 'react';
import PageContainer from '../../Components/PageContainer';
import CardContainer from '../../Components/CardContainer';

const Services = () => {

    const [services] = useState([{
        title: "Development",
        tags: ["design", "UX", "development"],
        img: './development.jpg',
    },{
        title: "UX Design",
        tags: ["development"],
        img: './ux.jfif',
    },{
        title: "Marketing",
        tags: ["marketing"],
        img: './marketing.jfif',
    },{
        title: "Branding",
        tags: ["marketing"],
        img: './branding.jfif',
    }]);

    return (
        <PageContainer>
            <p>We specialize in several services that will all help establish your business’s digital presence and identity.</p>
            <CardContainer>
                {services.map(service => <CardContainer.Card {...service} />)}
            </CardContainer>
        </PageContainer>
    )
}

export default Services;