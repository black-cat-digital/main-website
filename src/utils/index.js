export const setRangedInterval = (callback, min, max) => {
    let running = true;
    
    const interval = () => {
        setTimeout(() => {
            callback()
            if (running) {
                interval()
            }
        }, Math.floor(Math.random() * (max - min + 1) + min));
    }
    interval()

    return () => {
        running = false;
    }
}